while :
do
	if git checkout master &&
		git fetch origin master &&
		[ `git rev-list HEAD...origin/master --count` > 0 ]
	then
		say 'Updated!'
	fi
done
