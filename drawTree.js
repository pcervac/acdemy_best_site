// Draw Tree
        var canvas; //= document.getElementById("canvas");
        var ctx;
        var H, W;
        //Some variables
        var length, divergence, reduction, line_width, start_points = [];
        var img;
        var circles = [];

        function isIntersect(point, circle) {
            return Math.sqrt((point.x - circle.x) ** 2 + (point.y - circle.y) ** 2) < circle.radius;
        }


        function init() {
            img = document.createElement("img");
            img.setAttribute("src", "https://cdn0.iconfinder.com/data/icons/star-wars-3/154/star-wars-helmet-dart-weider-512.png");
            canvas = document.getElementById("canvas");
            ctx = canvas.getContext("2d");

            canvas.addEventListener('click', (e) => {
                 const mousePos = {
                    x: e.clientX,
                    y: e.clientY - canvas.offsetTop
                  };
                circles.forEach(circle => {
                        ctx = canvas.getContext("2d");
                        ctx.beginPath();
                        ctx.arc(circle.x, circle.y, circle.radius, 0, 2 * Math.PI, false);
                        ctx.lineWidth = circle.lineWidth;
                        ctx.strokeStyle = "brown";
                        ctx.stroke();
                    if (isIntersect(mousePos, circle)) {
                        ctx.beginPath();
                        ctx.arc(circle.x, circle.y, circle.radius, 0, 2 * Math.PI, false);
                        ctx.lineWidth = circle.lineWidth;
                        ctx.strokeStyle = "#37ad0f";
                        ctx.stroke();
                    }
                });
            });

            //Lets resize the canvas to occupy the full page
            W = window.innerWidth;
            H = window.innerHeight;
            canvas.width = W;
            canvas.height = H;
            //filling the canvas white
//            ctx.fillStyle = "white";
//            ctx.fillRect(0, 0, W, H);
            
            //Lets draw the trunk of the tree
            //lets randomise the variables
            //length of the trunk - 100-150
            length = 210; // + Math.round(Math.random()*50);
            //angle at which branches will diverge - 10-60
            divergence = 40; // + Math.round(Math.random()*50);
            //Every branch will be 0.75times of the previous one - 0.5-0.75
            //with 2 decimal points
            reduction = 0.69; //Math.round(50 + Math.random()*20)/100;
            //width of the branch/trunk
            line_width = 45;

            //This is the end point of the trunk, from where branches will diverge
            var trunk = {
                x: W / 2,
                y: length + 0,
                angle: 90
            };
            //It becomes the start point for branches
            start_points = []; //empty the start points on every init();
            start_points.push(trunk);

            //Y coordinates go positive downwards, hence they are inverted by deducting it
            //from the canvas height = H
            ctx.beginPath();
            ctx.moveTo(trunk.x, H - 50);
            ctx.lineTo(trunk.x, H - trunk.y);
            ctx.strokeStyle = "brown";
            ctx.lineWidth = line_width;
            ctx.stroke();
            branches();
        }

        //Lets draw the branches now
        function branches() {
            //reducing line_width and length
            length = length * reduction;
            line_width = line_width * reduction;
            ctx.lineWidth = line_width;

            var new_start_points = [];
            for (var i = 0; i < start_points.length; i++) {
                var sp = start_points[i];
                //2 branches will come out of every start point. Hence there will be
                //2 end points. There is a difference in the divergence.
                var ep1 = get_endpoint(sp.x, sp.y, sp.angle + divergence, length);
                var ep2 = get_endpoint(sp.x, sp.y, sp.angle - divergence, length);

                ctx.beginPath();
                //drawing the branches now
                ctx.moveTo(sp.x, H - sp.y);
                ctx.lineTo(ep1.x, H - ep1.y);
                ctx.moveTo(sp.x, H - sp.y);
                ctx.lineTo(ep2.x, H - ep2.y);
                ctx.moveTo(sp.x, H - sp.y);
                ctx.lineWidth = line_width;

                if (length < 10) {
                    ctx.strokeStyle = "green";
                    ctx.stroke();
                } else {
                    ctx.strokeStyle = "brown";
                    ctx.stroke();
                    if (length > 30) {
                        ctx.lineWidth = line_width / 5;
                        ctx.beginPath();
                        // all circles that are placed ...
                        circles.push({
                            x: sp.x,
                            y: H - sp.y,
                            radius: line_width * 2,
                            lineWidth: line_width / 5,
                            id: length
                        });
                        ctx.arc(sp.x, H - sp.y, line_width * 2, 0, 2 * Math.PI, false);
                        ctx.fillStyle = "#FFF";
                        ctx.fill();
                        ctx.stroke();
                    }
                    if (length > 40) {
                        ctx.drawImage(img, sp.x - line_width, H - sp.y - line_width, line_width * 2, line_width * 2);
                    }
                }
                //Time to make this function recursive to draw more branches
                ep1.angle = sp.angle + divergence;
                ep2.angle = sp.angle - divergence;

                new_start_points.push(ep1);
                new_start_points.push(ep2);
            }
            //Lets add some more color
            start_points = new_start_points;
            //recursive call - only if length is more than 2.
            //Else it will fall in an long loop
            if (length > 2) setTimeout(branches, 50);
            //else setTimeout(init, 500);
        }

        function get_endpoint(x, y, a, length) {
            //This function will calculate the end points based on simple vectors
            //http://physics.about.com/od/mathematics/a/VectorMath.htm
            //You can read about basic vectors from this link
            var epx = x + length * Math.cos(a * Math.PI / 180);
            var epy = y + length * Math.sin(a * Math.PI / 180);
            return {
                x: epx,
                y: epy
            };
        }
        
        // draw Tree