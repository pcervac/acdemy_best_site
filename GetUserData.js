var uid = "2a5e15a8dc0e9d8b39403e5b8688651d2f7e781e6b18647697763014f1559cc6";
var secret = "a5f0cf2d377fbe7b34216f96d0a95c9305b7cecf2150e6bb47ea5a445c34803a";
var token = "";
var itemJsonIndex = 0;
var maxUserCount = 0;
var allUsersId = [];

function _(id){
    return document.getElementById(id);
}


var userIndex = 313;
var allUserData = [];
var isset = true;

function toogleRequest(){
    isset = !isset;
    if (isset == false)
        getAllUserData();
}

// this function gets all user data by the user id got from getUserIds
function getAllUserData() {
    var xmlHttp = new XMLHttpRequest();
    _("apistatus").innerHTML = "Getting User data for: " + allUsersId[userIndex] + " | " + userIndex + "/" + allUsersId.length;
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
            var myjson = JSON.parse(xmlHttp.responseText);
            allUserData.push(myjson);
            userIndex++;
            if (allUsersId.length > userIndex) {
                console.log(myjson["cursus_users"].length)
                    uploadSingleUser(xmlHttp.responseText);
                    getAllUserData();
            } else {
                _("apistatus").innerHTML = "All User id's were got!";
                _("response").innerHTML = JSON.stringify(allUserData, null, 4);
            }
        }
    }
    xmlHttp.open("GET", "https://api.intra.42.fr/v2/users/"+allUsersId[userIndex], true); // false for synchronous request
    xmlHttp.setRequestHeader("Authorization", "Bearer " + token);
    xmlHttp.send(null);
}

function uploadSingleUser(data){
     var xmlHttp = new XMLHttpRequest();
    _("apistatus").innerHTML += "<br>sending User data...";
    
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
            _("response").innerHTML = xmlHttp.responseText;
        }
    }
    xmlHttp.open("POST", "UploadUserData.php", true); // false for synchronous request
    xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlHttp.send("user=" +data);
}

function getUserIds(theUrl) {
    var xmlHttp = new XMLHttpRequest();
    _("apistatus").innerHTML = "Getting User id's...";
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
            var myjson = JSON.parse(xmlHttp.responseText);
            for (var i = 0; i < myjson.length; i++) {
                allUsersId.push(myjson[i]["id"]);
            }
            if (myjson.length >= 30) {
//                console.log(myjson[29]['id']);
                getUserIds("https://api.intra.42.fr/v2/campus/6/users?range[id]=0," + (parseInt(myjson[29]['id']) - 1));
            } else {
                _("apistatus").innerHTML = "All User id's were got!";
                getAllUserData();
            }
        }
    }
    xmlHttp.open("GET", theUrl, true); // false for synchronous request
    xmlHttp.setRequestHeader("Authorization", "Bearer " + token);
    xmlHttp.send(null);
}


var pagenr = 1;
var toPrint = [];

function getAllProjects() {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
            var myjson = JSON.parse(xmlHttp.responseText);
            _("apistatus").innerHTML = "Getting All Projects";
            for (var i = 0; i < myjson.length; i++) {
                toPrint.push({
                    "id": myjson[i]["id"],
                    "name": myjson[i]["name"]
                });
            }
            if (pagenr == 1) {
                pagenr++;
                getAllProjects();
            } else {
                _("response").innerHTML = JSON.stringify(toPrint, null, 4);
                _("apistatus").innerHTML = "Done!";
            }
        }
    }
    console.log(pagenr);

    xmlHttp.open("GET", "https://api.intra.42.fr/v2/cursus/1/projects?page[number]=" + pagenr + "&page[size]=100", true); // false for synchronous request
    xmlHttp.setRequestHeader("Authorization", "Bearer " + token);
    xmlHttp.send(null);
}

function getToken() {
    var xmlHttp = new XMLHttpRequest();
    _("apistatus").innerHTML = "Getting API token...";
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
            token = JSON.parse(xmlHttp.responseText)['access_token'];
            _("apistatus").innerHTML = "Token got!";
            getUserIds("https://api.intra.42.fr/v2/campus/6/users?range[id]=0,999999");
        }
    }

    xmlHttp.open("POST", "https://api.intra.42.fr/oauth/token?" + "grant_type=client_credentials&client_id=" + uid + "&client_secret=" + secret, true);
    xmlHttp.send(null);
}

function httpGetURL() {
//    getToken();
    init();
}
