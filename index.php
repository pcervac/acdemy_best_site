
<html>
<head>
    <style>
        /*Some basic CSS*/
        
        * {
            margin: 0;
            padding: 0;
        }
        /*To remove the scrollers*/
        
        #canvas {
            position: absolute;
            top: 0;
            left: 0;
            display: block;
            width: 100%;
            height: 100%;
        }
        
        #response {
            position: fixed;
            display: block;
            top: 0;
            background-color: rgba(0,0,0,.8);
            color: white;
            padding: 10px;
            height: 98%;
            overflow-y: scroll;
            z-index: 100;
            font-size: 6pt;
        }
        
        #apistatus{
            position: absolute;
            right: 10px;
            top: 0;
            height: 50px;
            color: gray;
        }

    </style>
    <script src="drawTree.js" type="text/javascript"></script>
    <script src="GetUserData.js" type="text/javascript"></script>
</head>

<body onload="httpGetURL()">
    <canvas id="canvas"></canvas>
    <pre id="response" onclick="toogleRequest();" ></pre>
    <span id="apistatus"></span>
</body>

</html>
